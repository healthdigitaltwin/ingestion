#!/bin/bash

# Set the common name for the CA and server certificates
COMMON_NAME="localhost"

# Set the subject fields for the CA and server certificates
CA_SUBJECT="/C=UK/ST=London/L=London/O=HealthDigitalTwin/OU=Certificate Authority/CN=${COMMON_NAME}"
SERVER_SUBJECT="/C=UK/ST=London/L=London/O=HealthDigitalTwin/OU=Server/CN=${COMMON_NAME}"

# Set the output directory for the certificates
OUTPUT_DIR="../certs/"

# Create the output directory if it doesn't exist
mkdir -p "$OUTPUT_DIR"

# Generate the CA private key
openssl genrsa -out "$OUTPUT_DIR/ca.key" 4096

# Generate the CA certificate signing request (CSR)
openssl req -new -sha256 -key "$OUTPUT_DIR/ca.key" -out "$OUTPUT_DIR/ca.csr" -subj "$CA_SUBJECT"

# Generate the CA self-signed certificate
openssl x509 -req -in "$OUTPUT_DIR/ca.csr" -signkey "$OUTPUT_DIR/ca.key" -out "$OUTPUT_DIR/ca.crt" -days 3650 -sha256

# Generate the server private key
openssl genrsa -out "$OUTPUT_DIR/server.key" 2048

# Generate the certificate signing request (CSR) for the server certificate
openssl req -new -sha256 -key "$OUTPUT_DIR/server.key" -out "$OUTPUT_DIR/server.csr" -subj "$SERVER_SUBJECT"

# Create a configuration file for certificate extensions
cat > "$OUTPUT_DIR/extfile.cnf" <<EOF
subjectAltName = DNS:${COMMON_NAME}
extendedKeyUsage = serverAuth
EOF

# Generate the server certificate signed by the CA
openssl x509 -req -in "$OUTPUT_DIR/server.csr" -CA "$OUTPUT_DIR/ca.crt" -CAkey "$OUTPUT_DIR/ca.key" -CAcreateserial -out "$OUTPUT_DIR/server.crt" -days 3650 -sha256 -extfile "$OUTPUT_DIR/extfile.cnf"

# Clean up temporary files
rm "$OUTPUT_DIR/ca.csr" "$OUTPUT_DIR/server.csr" "$OUTPUT_DIR/extfile.cnf"

# Print the summary of generated files
echo "Certificate Authority (CA):"
echo "  - Private Key: ${OUTPUT_DIR}ca.key"
echo "  - Certificate: ${OUTPUT_DIR}ca.crt"
echo "Server Certificate:"
echo "  - Private Key: ${OUTPUT_DIR}server.key"
echo "  - Certificate: ${OUTPUT_DIR}server.crt"
