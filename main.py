# ./main.py

import ssl
import os
import asyncio
import logging
from fastapi import FastAPI, BackgroundTasks, HTTPException
from hl7apy.parser import parse_message
from hl7apy.exceptions import HL7apyException
from config.config import get_config
from app.hl7_validator import validate_hl7_message
from app.code_transformer import CodeTransformer
from app.kafka_producer import send_to_kafka, KafkaConnectionError, create_producer
from app.logger import get_logger
from pydantic import BaseModel
from typing import Optional
import uvicorn
from fastapi.openapi.docs import get_swagger_ui_html
from fastapi.openapi.utils import get_openapi

# Initialize the logger
logger = get_logger(__name__)

# Get the configuration
config = get_config()

# Get the lookup table file
transformer = CodeTransformer(config["LookupTable"]["CSV"]) if os.path.exists(config["LookupTable"]["CSV"]) else None

kafka_producer = None

class HL7Request(BaseModel):
    hl7_message: str        

app = FastAPI()

@app.on_event("startup")
async def startup_event():
    await init_kafka_producer()
    
    # start the MLLP server as a background task
    asyncio.create_task(start_mllp_server(config["MLLP"]["Host"], int(config["MLLP"]["Port"])))

@app.on_event("shutdown")
async def shutdown_event():
    if kafka_producer:
        await kafka_producer.stop()
    await asyncio.sleep(0.1)  # Add a small delay to allow pending tasks to complete

@app.post("/hl7")
async def hl7(hl7_message: str):
    logger.info("Received HL7 message over HTTP")

    # Remove leading/trailing white spaces and escape characters
    hl7_message = hl7_message.strip("\r\n")

    # Parse the HL7 message
    try:
        message = parse_message(hl7_message)
    except HL7apyException as e:
        logger.error(f"Error occurred while parsing HL7 message: {str(e)}")
        raise HTTPException(status_code=422, detail="HL7 message could not be parsed")

    # Validate the HL7 message
    is_valid = False
    if message is not None:
        is_valid = validate_hl7_message(hl7_message)

    # Send the HL7 message to Kafka
    await send_to_kafka(hl7_message.encode(), config["Kafka"]["Topic"], kafka_producer)
    logger.info("Message sent to Kafka")

    # Generate an ACK for the message
    ack_message = create_ack(message.msh if is_valid else None)

    # Return the ACK message
    return {"ack": ack_message}

@app.get("/docs", include_in_schema=False)
async def custom_swagger_ui_html():
    return get_swagger_ui_html(
        openapi_url=app.openapi_url,
        title=app.title + " - Swagger UI",
        oauth2_redirect_url=app.swagger_ui_oauth2_redirect_url,
        swagger_js_url="/static/swagger-ui-bundle.js",
        swagger_css_url="/static/swagger-ui.css",
    )

@app.get(app.openapi_url, include_in_schema=False)
async def get_open_api_endpoint():
    return get_openapi(
        title="Custom title",
        version="2.5.0",
        description="This is a very custom OpenAPI schema",
        routes=app.routes,
    )

async def init_kafka_producer():
    global kafka_producer
    if kafka_producer is None:
        kafka_producer = await create_producer()
        await kafka_producer.start()
    return kafka_producer

def create_ack(msh_segment, is_error=False):
    if is_error:
        ack_message = "MSH|^~\\&|{receiving_app}|{receiving_fac}|{sending_app}|{sending_fac}|{timestamp}||ACK^R01|{msg_control_id}|P|2.3\rMSA|AR|{msg_control_id}|Error occurred while processing the message"
    else:
        ack_message = "MSH|^~\\&|{receiving_app}|{receiving_fac}|{sending_app}|{sending_fac}|{timestamp}||ACK^R01|{msg_control_id}|P|2.3\rMSA|AA|{msg_control_id}"
    return ack_message.format(
        sending_app=msh_segment.msh_3.to_er7(),
        sending_fac=msh_segment.msh_4.to_er7(),
        receiving_app=msh_segment.msh_5.to_er7(),
        receiving_fac=msh_segment.msh_6.to_er7(),
        timestamp=msh_segment.msh_7.to_er7(),
        msg_control_id=msh_segment.msh_10.to_er7()
    )

async def handle_incoming_hl7(reader, writer):
    """Handles an incoming HL7 message."""
    data = await reader.read(10000)
    hl7_message = data.decode().strip("\x0b\x1c\x0d")
    logger.info("Message received")

    try:
        # Parse the HL7 message
        message = parse_message(hl7_message)
    except HL7apyException as e:
        logger.error(f"Error occurred while parsing HL7 message: {str(e)}")
        message = None  # Set message to None on parsing error

    # Validate the HL7 message
    is_valid = False
    if message is not None:
        is_valid = validate_hl7_message(hl7_message)

    # Send the HL7 message to Kafka for storage
    if is_valid:
        await send_to_kafka(hl7_message.encode(), config["Kafka"]["Topic"], kafka_producer)

        logger.info("Message sent to Kafka")

    # Generate an ACK for the message
    ack_message = create_ack(message.msh if is_valid else None)

    # Send the ACK message back to the client
    await writer.write(ack_message.encode() + b"\r\n")
    await writer.drain()
    logger.info("Ack sent, closing connection")

async def start_mllp_server(host, port):
    """Starts the MLLP server."""
    ssl_context = None
    if config["MLLP"]["EnableSSL"] == "True":
        ssl_context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
        ssl_context.load_cert_chain(certfile=config["MLLP"]["ClientCert"], keyfile=config["MLLP"]["ClientKey"])
        ssl_context.load_verify_locations(cafile=config["MLLP"]["CACert"])

    server = await asyncio.start_server(
        handle_incoming_hl7,
        host=host,
        port=port,
        ssl=ssl_context
    )

    logger.info(f"MLLP server started on host={host}, port={port}")
    await server.serve_forever()

                                                            
if __name__ == "__main__":
    try:
        uvicorn.run(
            "main:app",
            host=config["FastAPI"]["Host"],
            port=int(config["FastAPI"]["Port"]),
            ssl_keyfile=config["FastAPI"]["KeyFile"] if config["FastAPI"]["EnableSSL"] == "True" else None,
            ssl_certfile=config["FastAPI"]["CertFile"] if config["FastAPI"]["EnableSSL"] == "True" else None,
        )
        logger.info(f"HTTPS server started on host={config['FastAPI']['Host']}, port={config['FastAPI']['Port']}")
    finally:
        loop = asyncio.get_event_loop()
        loop.run_until_complete(shutdown_event())
        loop.close()

