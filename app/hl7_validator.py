#./app/hl7_validator.py

import logging
from hl7apy.core import Message, Segment
from hl7apy.parser import parse_message
from hl7apy.exceptions import HL7apyException
from config.config import get_config
from app.logger import get_logger

# Initialize the logger
logger = get_logger(__name__)

# Get the configuration
config = get_config()

def validate_hl7_message(hl7_message):
    """Validates an HL7 message.

    Args:
        hl7_message (str): The HL7 message to be validated.

    Returns:
        bool: True if the message is valid, False otherwise.
    """
    try:
        message = parse_message(hl7_message)

        # Check if the message has an MSH segment.
        if not message.MSH:
            logger.error(f"Invalid HL7 message: {hl7_message}")
            return False

        # Validate all Z segments in the message.
        for segment in message.children:
            if segment.name.startswith("Z"):
                if not segment.is_valid():
                    logger.error(f"Invalid Z segment: {segment.to_er7()}")
                    return False

        logger.info("HL7 message validation successful")
        return True

    except HL7apyException as e:
        logger.error(f"Error occurred while parsing HL7 message: {str(e)}")
        raise

    except Exception as e:
        logger.error(f"Unexpected error occurred while parsing HL7 message: {str(e)}")
        raise