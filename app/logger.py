import logging
from config.config import get_config

# Get the configuration
config = get_config()

def get_logger(name: str):
    # Initialize the logger
    logger = logging.getLogger(name)
    level = config["Logging"]["level"]

    if level == 'DEBUG':
        logger.setLevel(logging.DEBUG)
    elif level == 'INFO':
        logger.setLevel(logging.INFO)
    elif level == 'WARNING':
        logger.setLevel(logging.WARNING)
    elif level == 'ERROR':
        logger.setLevel(logging.ERROR)
    elif level == 'CRITICAL':
        logger.setLevel(logging.CRITICAL)
    else:
        logger.setLevel(logging.INFO)

    # Create a formatter with timestamp
    formatter = logging.Formatter("%(levelname)s:     %(name)s:  %(message)s")

    # Create a handler for console output
    c_handler = logging.StreamHandler()
    c_handler.setLevel(logging.DEBUG)
    c_handler.setFormatter(formatter)
    logger.addHandler(c_handler)

    return logger
