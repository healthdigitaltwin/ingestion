# ./app/kafka_producer.py

import ssl
from aiokafka import AIOKafkaProducer
from config.config import get_config
from app.logger import get_logger
from aiokafka.errors import KafkaError, KafkaConnectionError

logger = get_logger(__name__)

config = get_config()

# Create a SSL context only if SSL is enabled
ssl_context = None
if config["Kafka"]["EnableSSL"] == "True":
    ssl_context = ssl.create_default_context(
        purpose=ssl.Purpose.CLIENT_AUTH,
        cafile=config["Kafka"]["CACert"]
    )
    ssl_context.load_cert_chain(
        certfile=config["Kafka"]["ClientCert"],
        keyfile=config["Kafka"]["ClientKey"]
    )

# Initialize Kafka producer
PRODUCER = None

async def create_producer():
    if config["Kafka"]["EnableSSL"] == "True":
        producer = AIOKafkaProducer(
            bootstrap_servers=config["Kafka"]["BootstrapServers"],
            security_protocol="SSL",
            ssl_context=ssl_context
        )
    else:
        producer = AIOKafkaProducer(bootstrap_servers=config["Kafka"]["BootstrapServers"])

    await producer.start()
    return producer

async def send_to_kafka(message, topic, producer):
    try:
        if isinstance(producer, AIOKafkaProducer):
            await producer.send(topic, message)
        else:
            raise Exception("Kafka error occurred")
    except KafkaError as e:
        logger.error(f"An error occurred when sending to Kafka: {str(e)}")
        if str(e) == "ProducerClosed":
            logger.info("Attempting to recreate the producer...")
            PRODUCER = await create_producer()
            await PRODUCER.send(topic, message)
        else:
            raise KafkaConnectionError(str(e))