#./app/code_transformer.py

import csv
import os
from config.config import get_config
from app.logger import get_logger

# Get the configuration
config = get_config()

# Initialize the logger
logger = get_logger(__name__)

class CodeTransformer:
    """A class that transforms HL7 codes according to a lookup table.

    Args:
        lookup_table_path (str): The path to the lookup table file.
    """

    def __init__(self, lookup_table_path: str = None):
        self.lookup_table = {}
        self.load_lookup_table(lookup_table_path)

    def load_lookup_table(self, lookup_table_path: str) -> None:
        """Loads the lookup table from a file.

        Args:
            lookup_table_path (str): The path to the lookup table file.

        Raises:
            FileNotFoundError: If the lookup table file does not exist.
        """
        if not lookup_table_path or not os.path.isfile(lookup_table_path):
            logger.warning("Lookup table file does not exist.")
            return

        with open(lookup_table_path, newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                hl7_element = row['hl7_element']
                match_pattern = row['match_pattern']
                hl7_standard = row['hl7_standard']
                self.lookup_table[hl7_element] = (match_pattern, hl7_standard)

        logger.info("Lookup table loaded successfully.")

    def transform_code(self, code: str) -> str:
        """Transforms an HL7 code according to the lookup table.

        Args:
            code (str): The HL7 code to be transformed.

        Returns:
            str: The transformed HL7 code.
        """
        logger.info(f"Transforming code: {code}")
        if code not in self.lookup_table:
            logger.info(f"Code {code} not in lookup table, returning code unchanged.")
            return code

        match_pattern, hl7_standard = self.lookup_table[code]
        logger.info(f"Transformed code {code} to {hl7_standard}")
        return hl7_standard
