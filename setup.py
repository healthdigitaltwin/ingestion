from setuptools import setup, find_packages

setup(
    name="FlightDataRecorder_HL7",
    version="0.1.0",
    author="Robert Smith",
    author_email="smith_robert4@me.com",
    description="Flight Data Recorder HL7 Microservice",
    packages=find_packages(),
    install_requires=[
        "fastapi",
        "uvicorn",
        "hl7apy",
        "aiokafka",
        "confluent-kafka",
        # Add other dependencies here
    

    ],
    entry_points={
        "console_scripts": [
            "flightdatarecorder=main:main"
        ]
    },
)
