import configparser

class CaseSensitiveConfigParser(configparser.ConfigParser):
    def optionxform(self, optionstr):
        return optionstr

def get_config():
    config = CaseSensitiveConfigParser()
    config.read('./config/config.ini')

    config_dict = {}
    for section in config.sections():
        config_dict[section] = config[section]

    return config_dict

