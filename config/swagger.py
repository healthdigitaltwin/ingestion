from fastapi.openapi.utils import get_openapi

def generate_openapi_schema(app):
    openapi_schema = get_openapi(
        title="HL7 API",
        version="1.0.0",
        description="This API allows you to send HL7 messages over HTTP.",
        routes=app.routes,
    )
    # Modify the OpenAPI schema with your custom details
    openapi_schema["info"]["x-logo"] = {
        "url": "https://example.com/logo.png",
        "altText": "HL7 API Logo",
    }
    openapi_schema["info"]["contact"] = {
        "name": "Your Name",
        "email": "your.email@example.com",
    }
    openapi_schema["info"]["license"] = {
        "name": "MIT License",
        "url": "https://opensource.org/licenses/MIT",
    }
    # Add more customizations as needed
    return openapi_schema