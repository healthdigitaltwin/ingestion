# ./tests/test_main.py

import os
import sys
import pytest
from httpx import AsyncClient
# Get the current directory
current_dir = os.path.dirname(os.path.abspath(__file__))

# Get the parent directory by going up one level
parent_dir = os.path.dirname(current_dir)

# Add the parent directory to the Python path
sys.path.append(parent_dir)
from main import app


@pytest.mark.asyncio
async def test_hl7_endpoint():
    async with AsyncClient(app=app, base_url="http://testserver") as client:
        valid_message = {
            "hl7_message": "MSH|^~\\&|SendingApplication|SendingFacility|ReceivingApplication|ReceivingFacility|20230703120000||ADT^A01|1234567890|P|2.5|\rEVN|A01|20230703120000||\rPID|1||12345||Doe^John||19800101|M|\rPV1|1|I|ICU^101^1||||123^Smith^John^Dr.|||ADM|A0|"
        }

        # Test a valid HL7 message
        response = await client.post("/hl7", json=valid_message)
        if response.status_code == 200:
            data = response.json()
            assert "ack" in data
            ack_message = data["ack"]
            assert isinstance(ack_message, str)  # Verify that the ACK message is a string

        # Test an invalid HL7 message
        invalid_message = {
            "hl7_message": "InvalidHL7Message"
        }
        response = await client.post("/hl7", json=invalid_message)
        if response.status_code == 422:
            data = response.json()
            assert "ack" not in data

        # TO DO: Test missing HL7 message field 
        missing_message = {}
        response = await client.post("/hl7", json=missing_message)
        if response.status_code == 422:
            data = response.json()
            assert "ack" not in data


@pytest.mark.asyncio
async def test_docs_endpoint():
    async with AsyncClient(app=app, base_url="http://testserver") as client:
        response = await client.get("/docs")
        assert response.status_code == 200
        assert "Swagger UI" in response.text


# Add more tests for other endpoints and functionality as needed


# Run the tests
if __name__ == "__main__":
    pytest.main(["-v"])
