# ./tests/test_hl7_validator

import pytest
import os
import sys

# Get the current directory
current_dir = os.path.dirname(os.path.abspath(__file__))

# Get the parent directory by going up one level
parent_dir = os.path.dirname(current_dir)

# Add the parent directory to the Python path
sys.path.append(parent_dir)


from app.hl7_validator import validate_hl7_message

# Test valid HL7 message
def test_valid_hl7_message():
    hl7_message = "MSH|^~\\&|SendingApp|SendingFac|ReceivingApp|ReceivingFac|20230704120000||ADT^A08|MSGID123456789|P|2.5|||NE|AL|USA|||20230704120000\nPID|1||12345||Doe^John^D||19800101|M|||123 Main St^Apt 4B^New York^NY^10001||(123)456-7890\n"
    assert validate_hl7_message(hl7_message) == True

# Test HL7 message without MSH segment
def test_invalid_hl7_message_without_msh():
    hl7_message = "PID|1||12345||Doe^John^D||19800101|M|||123 Main St^Apt 4B^New York^NY^10001||(123)456-7890\n"
    with pytest.raises(Exception) as e:
        validate_hl7_message(hl7_message)
    assert str(e.value) == "Invalid message"

# # TO DO Test HL7 message with invalid Z segment
# def test_invalid_hl7_message_with_invalid_z_segment():
#     hl7_message = "MSH|^~\\&|SendingApp|SendingFac|ReceivingApp|ReceivingFac|20230704120000||ADT^A08|MSGID123456789|P|2.5|||NE|AL|USA|||20230704120000\nPID|1||12345||Doe^John^D||19800101|M|||123 Main St^Apt 4B^New York^NY^10001||(123)456-7890\nZXX|123\n"
#     assert validate_hl7_message(hl7_message) == False

# Test HL7 message with parsing error
def test_invalid_hl7_message_with_parsing_error():
    hl7_message = "Invalid Message"
    with pytest.raises(Exception):
        validate_hl7_message(hl7_message)