# ./tests/test_kafka_producer.py

import os
import sys

# Get the current directory
current_dir = os.path.dirname(os.path.abspath(__file__))

# Get the parent directory by going up one level
parent_dir = os.path.dirname(current_dir)

# Add the parent directory to the Python path
sys.path.append(parent_dir)

import pytest
from unittest.mock import patch, Mock
from app.kafka_producer import create_producer, send_to_kafka
from aiokafka import AIOKafkaProducer
from config.config import get_config

# Get the configuration
config = get_config()

@pytest.fixture
def mock_get_config():
    with patch("app.kafka_producer.get_config") as mock:
        yield mock

@pytest.mark.asyncio
async def test_create_producer_with_ssl():
    config = get_config()

    with patch("ssl.create_default_context") as mock_ssl_context:
        with patch("app.kafka_producer.AIOKafkaProducer.start") as mock_start:
            producer = await create_producer()
            if config["Kafka"]["EnableSSL"] == "True":
                mock_ssl_context.assert_called_once()
            assert isinstance(producer, AIOKafkaProducer)
            mock_start.assert_called_once()

@pytest.mark.asyncio
async def test_create_producer_without_ssl():
    config = get_config()

    with patch("app.kafka_producer.AIOKafkaProducer.start") as mock_start:
        producer = await create_producer()
        assert isinstance(producer, AIOKafkaProducer)
        mock_start.assert_called_once()

@pytest.mark.asyncio
async def test_send_to_kafka():
    mock_producer = Mock(spec=AIOKafkaProducer)
    await send_to_kafka("test_message", "test_topic", mock_producer)
    mock_producer.send.assert_called_once_with("test_topic", "test_message")


# more tests...