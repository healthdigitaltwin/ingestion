import os
import sys
import pytest
import csv

# Get the current directory
current_dir = os.path.dirname(os.path.abspath(__file__))

# Get the parent directory by going up one level
parent_dir = os.path.dirname(current_dir)

# Add the parent directory to the Python path
sys.path.append(parent_dir)

from app.code_transformer import CodeTransformer

@pytest.fixture
def lookup_table_csv(tmpdir):
    csv_data = [
        {'hl7_element': 'A', 'match_pattern': '1', 'hl7_standard': 'A1'},
        {'hl7_element': 'B', 'match_pattern': '2', 'hl7_standard': 'B2'},
    ]
    csv_path = tmpdir.join('lookup_table.csv')
    with open(csv_path, 'w', newline='') as csvfile:
        fieldnames = ['hl7_element', 'match_pattern', 'hl7_standard']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(csv_data)
    return csv_path

def test_load_lookup_table(lookup_table_csv):
    transformer = CodeTransformer(lookup_table_path=lookup_table_csv)
    assert len(transformer.lookup_table) == 2

def test_transform_code(lookup_table_csv):
    transformer = CodeTransformer(lookup_table_path=lookup_table_csv)
    assert transformer.transform_code('A') == 'A1'
    assert transformer.transform_code('B') == 'B2'
    assert transformer.transform_code('C') == 'C'  # Code not in lookup table, should be unchanged
