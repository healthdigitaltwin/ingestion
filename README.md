# Flight Data Recorder HL7 Microservice

The Flight Data Recorder HL7 microservice receives HL7 v2.5 messages, validates them, performs optional code transformations, and sends the transformed data to a Kafka queue. It can run in different environments such as IDE, Docker, and Kubernetes using CentOS.

This app receives HL7 messages from two sources: a REST API and an MLLP client. The app validates the HL7 messages and then transforms any codes that match a pattern in a lookup table. The transformed messages are then sent to a Kafka topic.

The app is written in Python and uses the following libraries:

- FastAPI
- hl7apy
- confluent-kafka

The app is configured using a configuration file called config.ini. The configuration file specifies the following:

- The hostname and port of the REST API
- The hostname and port of the MLLP client
- The topic name for the Kafka topic
- The path to the lookup table

## The app can be run using the following command:

```uvicorn main:app --host 0.0.0.0 --port 8000```

The app also includes a script called create_topics.py that can be used to create the Kafka topic. To run the script, use the following command:

```python create_topics.py```

## Features

- Receives HL7 v2.5 messages on a TLS endpoint and via a HTTPS REST endpoint
- Validates the received messages to ensure their validity
- Performs optional code transformations based on a lookup table
- Sends the transformed data to a Kafka queue with 9 partitions and 3 replicas
- Stores the transformed data in a JSON format with specific fields extracted from the HL7 message

## Requirements

- Python 3.7+
- Docker
- Kubernetes
- CentOS

## Explanation of the folder structure

```
FlightDataRecorder_HL7/
├── app/
│   ├── __init__.py
│   ├── hl7_validator.py
│   ├── code_transformer.py
│   └── kafka_producer.py
├── config/
│   └── config.ini
├── tests/
│   ├── test_hl7_validator.py
│   ├── test_code_transformer.py
│   └── test_kafka_producer.py
├── docker/
│   └── Dockerfile
├── kubernetes/
│   └── deployment.yaml
├── README.md
├── requirements.txt
└── main.py
```

| File/Folder            | Description                                                                                              |
|------------------------|----------------------------------------------------------------------------------------------------------|
| app/                   | Contains the main application code.                                                                      |
| └── __init__.py        | Initializes the application.                                                                             |
| └── hl7_validator.py   | Implements the HL7 message validation logic.                                                              |
| └── code_transformer.py| Handles code transformations based on the lookup table.                                                   |
| └── kafka_producer.py  | Sends the transformed data to the Kafka queue.                                                           |
| config/                | Contains the configuration files.                                                                        |
| └── config.ini         | Stores the configuration settings such as TLS certificates, Kafka cluster configuration, and lookup table.|
| tests/                 | Contains the unit tests for the application.                                                             |
| └── test_hl7_validator.py    | Unit tests for the HL7 message validation.                                                           |
| └── test_code_transformer.py | Unit tests for the code transformation functionality.                                                |
| └── test_kafka_producer.py   | Unit tests for the Kafka producer.                                                                  |
| docker/                | Contains the Docker-related files.                                                                       |
| └── Dockerfile          | Defines the Docker image configuration.                                                                  |
| kubernetes/            | Contains the Kubernetes-related files.                                                                   |
| └── deployment.yaml     | Defines the Kubernetes deployment configuration.                                                        |
| README.md              | Provides information about the project, installation instructions, and usage guide.                     |
| requirements.txt       | Lists the required dependencies for the project.                                                        |
| main.py                | Entry point of the application.                                                                          |


## Contributing

Contributions are welcome! Please fork the repository and submit a pull request.

## License

This project is licensed under the Apache 2.0 licence